# iOS app securing safety of movement

## Struktura práce

- Úvod
- Cíl práce
- Analýza
    - Cílová skupina
        - Definice cílové skupiny
        - Chování cílové skupiny
        - Shrnutí
    - Existující řešení
        - Mobilní aplikace
        - Volání a infolinky
        - Shrnutí
    - Požadavky
        - Mindmapa
        - Funkční
        - Nefunkční
- Realizace
    - Návrh        
        - Návrh designu
        - Volba technologií
        - Návrh 
    - Implementace
        - Server
        - iOS Aplikace
    - Nasazení
- Testování
    - Uživatelské testování
    - Výsledky testování a vylepšení

## Changelog

### Aktuálně:

- Základní návrh aplikace

### Provedeno:

- Rozhovory s cílovou skupinou
- Rešerše existujících řešení
- Specifikace funkcionalit

## Harmonogram

### Vysvětlivky

- ✅ - Hotovo
- 🚼 - Rozpracováno
- 🆘 - Nezačato
- (datum) - Předpokládaný datum dokončení

### Úkoly mimo harmonogram

- 🚼 Klíčová slova
- 🆘 Abstrakt
- 🆘 Úvod
- 🆘 Závěr
- 🆘 Kontrola citací
- 🆘 Kontrola formalit
- 🆘 Odevzdávní práce

### Timeline

- ✅ (21.02.2022) Rozhovory s potenciálními uživateli ohledně bezpečnosti při cestování a analýza chování uživatele
- 🚼 (21.02.2022) Řešerše stávajících řešení
- 🚼 (21.02.2022) Navržená funkcionalita
- 🚼 (3.03.2022) Návrh aplikace
  - 🚼 (24.02.2022) Design
  - 🆘 (24.02.2022) Průzkum technologií
  - 🆘 (03.03.2022) Návrh architektury
  - 🆘 (03.03.2022) Diagram tříd
- 🆘 (17.03.2022) Implementace
  - 🆘 (17.03.2022) Serverová část
  - 🆘 (17.03.2022) iOS Aplikace
- 🆘 (24.03.2022) Nasazení na App Store
- 🆘 (07.04.2022) Testování
  - 🆘 (07.04.2022) Uživatelské testování v reálném světě
- 🆘 (14.04.2022) Zhodnocení výsledků

